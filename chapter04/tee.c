/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#define BUFFER_SIZE                 4096

static void     version(void);
static void     usage(void);
/* 
 * Exercise 4.1:
 *
 * The tee command reads its standard input until EOF, writing a copy of
 * the input to standard output and to the file named in its command line
 * argument. Implement tee using I/O system calls. By default, tee 
 * overwrites any existing file with the given name. Implement the -a
 * commandline option (tee -a file), which causes tee to append text to the
 * end of a file if it already exists.
 */
int
main(int argc, char **argv)
{
    char buf[BUFFER_SIZE];
    mode_t mask;
    ssize_t rdsz;
    int ch;
    int read_bytes;
    int openflags; 
    int openperms;
    int append;
    int fd;

    append = 0;                          /* by default do not overwrite */
    read_bytes = 0;
    openflags = O_WRONLY | O_CREAT;
    mask = umask(0);
    openperms = 01777 ^ mask;
    mask = umask(mask);

    printf("perms: %u", (unsigned)openperms);
    while ((ch = getopt(argc, argv, "ahv")) > 0) {
        switch (ch) {
        case 'a':
            append = 1;
            break;
        case 'h':
            usage();
            exit(EX_USAGE);
        case 'v':
            version();
            exit(EX_USAGE);
        default:
            /* NOTREACHED */
            break;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 1) {
        usage();
        exit(EX_USAGE);
    }


    if (append)
        openflags |= O_APPEND;
    else
        openflags |= O_TRUNC;

    if (-1 == (fd = open(argv[0], openflags, openperms)))
        err(EX_DATAERR, "%s", argv[0]);
    
    errno = 0;
    while ((rdsz = read(STDIN_FILENO, buf, BUFFER_SIZE)) > 0) {
        if ((-1 == write(STDOUT_FILENO, buf, rdsz)) || 
            (-1 == write(fd, buf, rdsz))) {
            warn("write failed");
            break;
        }
        
        read_bytes += rdsz;
    }

    if (-1 == close(fd))
        warn("%s", argv[0]);

    return errno;
}


/*
 * print usage information
 */
void
usage()
{
    version();
    printf("usage: tee [-a] file\n");
    printf("    -a      append to file");
    printf("\nechoes all stdin to stdout and file.\n");
}


/*
 * print version information
 */
void
version()
{
    printf("tee version 1.0.0");
}

