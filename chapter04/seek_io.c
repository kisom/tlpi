/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#include <sys/stat.h> 

#include <ctype.h>
#include <err.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>


static void      usage(void);
static void      version(void);


extern char     *__progname;


/*
 * reimplementation of example 4-3, a demonstration of read(), write(), and
 * lseek().
 */
int
main(int argc, char **argv)
{
    ssize_t rdsz, wrsz;
    size_t j, len;
    char *buf, *filename;
    off_t offset;
    int fd;
    int ap;
    int ch;

    while ((ch = getopt(argc, argv, "hv")) > 0) {
        switch(ch) {
        case 'h':
            usage();
        case 'v':
            version();
            exit(EX_USAGE);
        default:
            /* NOTREACHD */
            usage();
        }
    }

    argc -= optind;
    argv += optind;

    if (argc < 2)
        usage();

    filename = argv[0];

    if (-1 == (fd = open(filename, O_RDWR | O_CREAT, 0755)))
        err(EX_IOERR, "%s", argv[0]);

    argv++;
    argc--;

    for (ap = 0; ap < argc; ++ap) {
        switch (argv[ap][0]) {
        case 'r':
        case 'R':
            len = strtoul(&argv[ap][1], NULL, 0); 
            buf = calloc(len, sizeof(char));
            if (NULL == buf)
                err(EX_OSERR, "calloc() failed");

            rdsz = read(fd, buf, len);
            if (-1 == rdsz) {
                free(buf);
                buf = NULL;
                err(EX_IOERR, "read() failed");
            } else {
                printf("%s: ", argv[ap]);
                for (j = 0; j < len; ++j) {
                    if ('r' == argv[ap][0])
                        printf("%c", isprint((unsigned char)buf[j]) ? 
                                     buf[j] : '?');
                    else
                        printf("%02x ", (unsigned int)buf[j]);
                }

                printf("\n");
            }

            free(buf);
            buf = NULL;
            break;
        case 'w':
            wrsz = write(fd, &argv[ap][1], strlen(&argv[ap][1]));
            if (-1 == wrsz)
                err(EX_IOERR, "%s", filename);
            printf("%s: wrote %ld bytes\n", argv[ap], (long)wrsz);
            break;
        case 's':
            offset = strtoul(&argv[ap][1], NULL, 0);
            if (-1 == lseek(fd, offset, SEEK_SET))
                err(EX_IOERR, "%s", filename);
            else
                printf("%s: seek succeeded\n", argv[ap]);
            break;
        default:
            usage();
        }
    }
    return EX_OK;
}


/*
 * print usage information
 */
void
usage()
{
    version();
    printf("%s usage: %s file commands\n", __progname, __progname);
    printf("commands:\n");
    printf("    r<n>        read n bytes, and output the raw characters\n");
    printf("    R<n>        read n bytes, and output the hex values\n");
    printf("    w<str>      write str to file\n");
    printf("    s<n>        seek n bytes forward in file\n");
    exit(EX_USAGE);
}


/*
 * print version information
 */
void
version()
{
    printf("%s version 1.0.0", __progname);
}
