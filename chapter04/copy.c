/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <sysexits.h>
#include <unistd.h>

#ifndef COPY_CHUNK
#define COPY_CHUNK              4096
#endif

/*
 * copy the file specified first on the command line to the file specified
 * second. this is my implementation of example 4-1, done slightly differently
 * than in the book. i chose to not allow overwriting files.
 */
int 
main(int argc, char **argv)
{
    char buf[COPY_CHUNK];
    int sourcefd, read_flags;
    int targetfd, write_flags;
    mode_t source_perms;
    ssize_t readsize;

    if (argc != 3)
       errx(EX_USAGE, "usage: %s old-file new-file", argv[0]);

    read_flags   = O_RDONLY;
    write_flags  = O_WRONLY | O_CREAT | O_EXCL;
    source_perms =  S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

    sourcefd = open(argv[1], read_flags, source_perms);
    if (-1 == sourcefd)
        err(EX_DATAERR, "%s", argv[1]);

    targetfd = open(argv[2], write_flags, source_perms);
    if (-1 == targetfd)
        err(EX_DATAERR, "%s", argv[2]);

    while ((readsize = read(sourcefd, buf, COPY_CHUNK)) > 0) {
        if (readsize != write(targetfd, buf, readsize))
            warn("mismatched r/w!");
    }

    if (-1 == close(sourcefd))
        warn("error closing %s", argv[1]);
    if (-1 == close(targetfd))
        warn("error closing %s", argv[2]);

    return EX_OK;
}
