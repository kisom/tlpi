subdirs := chapter04 chapter05 chapter06 chapter07 chapter08

all: $(subdirs)

clean: 
	@for chap in $(subdirs); do (printf "$$chap: make clean -> " && \
	cd $$chap && make clean); done

$(subdirs):
	@echo "building $@"
	-@cd $@ && make

.PHONY: all clean $(subdirs)
