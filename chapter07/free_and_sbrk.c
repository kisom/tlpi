#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#define MAX_ALLOCS 1000000
int
main(int argc, char *argv[])
{
	char           *ptr[MAX_ALLOCS];
	int             freeStep, freeMin, freeMax, blockSize, numAllocs,
	                j;

	printf("\n");

	if (argc < 3 || strcmp(argv[1], "--help") == 0)
		err(EX_USAGE, "%s num-allocs block-size [step [min [max]]]\n", 
                    argv[0]);
	numAllocs = strtol(argv[1], NULL, 10);
	if (numAllocs < 0 || numAllocs > MAX_ALLOCS)
	        err(EX_USAGE, "num-allocs > %d\n", MAX_ALLOCS);
	blockSize = strtol(argv[2], NULL, 0);
	freeStep = (argc > 3) ? strtol(argv[3], NULL, 10) : 1;
	freeMin = (argc > 4) ? strtol(argv[4], NULL, 10) : 1;
	freeMax = (argc > 5) ? strtol(argv[5], NULL, 10) : numAllocs;
	if (freeMax > numAllocs)
		err(EX_USAGE, "free-max > num-allocs\n");
	printf("Initial program break: %10p\n", sbrk(0));
	printf("Allocating %d*%d bytes\n", numAllocs, blockSize);
	for (j = 0; j < numAllocs; j++) {
		ptr[j] = malloc(blockSize);
		if (ptr[j] == NULL)
			err(EX_OSERR, "malloc");
                else
                        printf("Program break is now: %10p\n ", sbrk(0));
	}
	printf("Program break is now: %10p\n ", sbrk(0));
	printf("Freeing blocks from %d to %d in steps of %d\n",
	       freeMin, freeMax, freeStep);
	for (j = freeMin - 1; j < freeMax; j += freeStep)
		free(ptr[j]);
	printf("After free(), program break is: %10p\n", sbrk(0));
	exit(EXIT_SUCCESS);
}
