process: instance of executing program
program: describes how to construct a process at run time
    - binary format identification
    - machine-language instructions
    - program entry-point address
    - data (values to initialise variables
    - symbol / relocation tables
    - shared-library / dynamic linking information

memory layout of a process
    - text segment
    - data segment: initialised global / static vars
    - bss segment: uninitialised global / static vars
    - stack: stack frames store function's local variables, args, and
      return value.
    - heap: memory for variables that can be dynamically allocated at
      run time; top of heap is called the program break.

locality of reference: 
    spatial locality: tendency of programs to access memory proximate to
    those recently accessed due to continguous instructions (sequential
    processing of instructions) or sequential processing of data structures.

    temporal locality: tendency of programs to access recently-accessed memory
    locations, i.e. due to loops.

virtual memory:
    split program memory into into small fixed-size units called pages;
    RAM is split into a series of page frames of fixed size. at any given time,
    only some pages need to be resident in physical memory page frames (i.e.
    the resident set). other pages are in swap (the swap page set).

    a page fault occurs when a program attempts to access a non-resident
    page; the kernel suspends the process while the page is swapped in
    (loaded from the swap area to physical memory).

    the kernel maintains a page table that describes the location of each
    page in the process's virtual address space (the set of all virtual
    memory pages accessible by the process). a SIGSEGV is generated if a
    process attempts to access a page that is not in the page table. the
    page table may be modified (including entries added and removed) in
    several cases, including:
        * pages are swapped in or out of physical memory,
        * stack grows downward beyond limits previously reached,
        * memory is allocated / deallocated on the heap,
        * when SysV shared memory is attached / detached, or
        * memory is mapped or unmapped.

    virtual memory addresses do not correspond to physical memory addresses.
    this design allows:
        * processes are isolated, both from each other and from the kernel.
        * when appropriate, processes can share memory - for example, two
          processes running the same program can share a read-only text area,
          or via shared memory (shmget) or mapped memory (mmap).
        * pages may be marked with permissions, such as read-only or 
          executable. different processes accessing the memory may have
          different permissions for each page.
        * the programmer and development tools (compiler, linker, etc...)
          do not have to be aware of the physical layout of RAM.
        * by only loading required pages into memory, the program starts up
          faster (swapping is a performance hit); futhermore, programs may
          have a larger memory footprint than the available RAM.
        * generally, CPU usage is optimised, as a smaller footprint for
          processes means more processed may run; this increases the chance
          that there is a process the CPU can run at any given time.

stack and stack frames:
    stack pointer points to end of stack. stack grows and shrinks linearly as 
    functions are called; in most implementations, the stack segment itself
    does not shrink.

    the kernel stack is a similar construct used when making system calls, as
    the user stack is in unprotected (usermode) memory.

    user stack frame:
        - function arguments
        - local variables
        - call linkage information (i.e. old ip)

    over a program's life, stack frames are created and destroy. ex., given
    segment.c:

       [ main ] ->
       [ main, calculate ] ->
       [ main, calculate, square ]

command line args:
    can use argv[0] to determine program action based on the name used to
    invoke the program. for example, gzip(1), gunzip(1), and zcat(1) all
    refer to the same executable; the appropriate behaviour is automatically
    selected based on the name used. the main binary (gzip) still supports
    explicit behaviour selection via command line options.

    non-portable means of accessing the command line information:
        * in Linux, /proc/PID/cmdline provides CLI args. each arg is separated
        by a null byte.
        * GNU C library provides 'program_invocation_name' and
        'program_invocation_short_name' as global variables
        * __progname is provided as an external variable on many systems

    argv and environ reside in the a single contiguous area of memory
    just above process stack; ARG_MAX declares what the maximum
    number of bytes that can be stored here; this may also be obtained with
    sysconf(_SC_ARG_MAX). as of SUSv3 it is unspecified whether an
    implementation counts overhead bytes (i.e. NULL terminators, alignment
    padding, and argv/environ arrays of pointers) against ARG_MAX, but
    guarantees a minimum of 4096 bytes.

environment list:
    the environment variable, a definition of the form name=value that
    can be used to store arbitrary information. new processes inherit their
    parent's environment, and may be a form of primitive IPC.

    in linux, '/proc/PID/environ' specifies the environment; each kv pair
    is separated by a NUL byte.

    the global variable **environ points to a NULL-terminated list of pointers
    to null-terminated strings.

    some implementations allow a third argument to main:
        int
        main(int argc, char *argv[], char *envp[])
    however, this is not specified by SUSv3.

    getenv (stdlib,h) is used to retrieve the value of an environment variable.
    the string returned may not be freed or modified as it may be a pointer 
    into the environment; alternatively, the implementation may use a
    statically allocated buffer overwritten on subsequent calls to environment
    functions (getenv, putenv, setenv, unsetenv).

    putenv is used to create a new environment variable, setenv may also be 
    used to modify a current variable. note that the string used in putenv is
    part of the environment: if the string is modified, it will affect the
    process environment; the string should not be an automatic variable,
    as the stack is overwritten on return. putenv returns nonzero on error.
    glibc implementation contains a nonstandard extension: if the string
    doesn't contain an '=', the key named by the string will be removed.

    setenv creates a new memory buffer, and therefore the strings passed
    in are not used past the function call and automatic variables are safe.

    setenv and unsetenv are new to SUSv3 and came from BSD.

    the environment may be cleared either by setting environ to NULL
    or by calling clearenv, a common function that is not specified by
    SUSv3.

nonlocal gotos:
    setjmp and longjmp are used to perform nonlocal gotos. nonlocal means that
    the target is a location outside the currently executing function. this
    restriction occurs because all C functions reside at the same scope level.
    
    setjmp establishes the target; after a call to longjmp, it will look as
    if we have returned from setjmp a second time. the jumps are distinguished
    by the value returned from the function: zero on initial call (setting
    the target), and nonzero on subsequent jumps (the value passed by the
    longjmp). longjmp will change 0 values to 1. the env variable required
    by both contains the context information required to complete the jump.

    the env var stories a copy of the PC and SP, allowing longjmp to:
        * strip off stack frames between the two invocations (unwind the
        stack) and
        * reset the PC so the program continues execution form the original
        setjmp location.

    SUSv3 restricts nonlocal gotos so they may only appear:
        * as entire controlling expression of a selection/iteration statement
        (if, switch, while...),
        * as the operand of a unary ! operator, where resulting expression
        is entire controlling expression as above,
        * as part of a comparison where the other operand is an integer
        constant expression and the first bullet applies,
        * as a free-standing function call that is not embedded as part
        of a larger expression.

        assignment does not apply above: s = setjmp(env) is wrong.

        these restrictions exist to ensure no expression is involved that
        requires temporary storage that might be disrupted or lost by
        the call.

        the following sequence (assuming a global env buffer) is possible:
            1. call x() that uses setjmp to establish a target in env
            2. return from x
            3. call y() that does a longjmp using env
        this is an error: longjmp must not be used to jump into a function
        that has returned. the stack will attempt to unwind to a nonexistent
        frame yielding much weeping and gnashing of teeth. behaviour is
        undefined if longjmp is called inside a signal handler.

        optimising compilers may reorder instructions and store certain
        variables in registers; as nonlocal gotos are established and
        executed at runtime, the compiler cannot take them into account.
        optimised variables may have incorrect values as a result. this can
        be avoided by using the 'volatile' keyword.

        avoid nonlocal gotos.
