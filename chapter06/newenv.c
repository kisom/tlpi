/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern char **environ;

/*
 * setenv
 *      int setenv(const char *name, const char *value, int overwrite);
 * The  setenv()  function  adds  the  variable  name  to the environment with 
 * the value value, if name does not already exist. If name does exist in the 
 * environment, then its value is changed to value if overwrite is nonzero; if 
 * overwrite is zero, then the value of name is not changed. This function 
 * makes copies of the strings pointed to by name and value (by contrast with 
 * putenv(3)).
  */
int
new_setenv(const char *name, const char *value, int overwrite)
{
            char *envline;
            char *env;
            size_t env_len;
    
            if (0 == overwrite && NULL != getenv(name))
                return 0;
    
            /* 2 accounts for '=' and NUL terminator */
            env_len = strlen(name) + strlen(value) + 2;
            envline = calloc(env_len, sizeof envline);
            if (NULL == envline)
                return -1;
    
            snprintf(envline, env_len, "%s=%s", name, value);

            /*
             * do not free envline: as per the manpage for putenv(3):
             * The string pointed to by string becomes part of the environ‐
             * ment, so altering the string changes the environment.
             */
            return putenv(envline);
}


/*
 * unsetenv
 *      int unsetenv(const char *name);
*/
int
new_unsetenv(const char *name)
{
            char **envp, **movep;
            char *search_key;
            size_t name_len;
    
            if (NULL == getenv(name))
                return 0;
    
            envp = environ;
            name_len = strlen(name);
            while (NULL != getenv(name) && envp != NULL) {
                if (0 == strncmp(name, *envp, name_len) && 
                    (*envp)[name_len] == '=') {
                    movep = envp;
                    while (*movep != NULL) {
                        *movep = *(movep + 1);
                        movep++;
                    }
                } else
                    envp++;
            }
            return NULL == getenv(name) ? 0 : -1;
}


int
main(int argc, char **argv)
{
            char **envp = environ;

            printf("[+] clearing environment...\n");
            clearenv();
            printf("[+] creating marker environment variable:\n");
            new_setenv("MARKER", "test01", 0);
            printf("[+] checking value of MARKER: %s\n", getenv("MARKER"));

            printf("[+] setting marker with no overwrite:\n");
            new_setenv("MARKER", "test02", 0);
            printf("[+] checking value of MARKER: %s\n", getenv("MARKER"));

            printf("[+] setting marker with overwrite:\n");
            new_setenv("MARKER", "test03", 1);
            printf("[+] chceking value of MARKER: %s\n", getenv("MARKER"));

            printf("[+] calling unsetenv on marker...\n");
            if (-1 == new_unsetenv("MARKER"))
                perror("[!] new_unsetenv");
            printf("[+] checking value of MARKER: %s\n", getenv("MARKER"));
            return 0;
}
