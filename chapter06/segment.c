/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>

#include <stdio.h>
#include <stdlib.h>


        char    global_buffer[65536];               /* stored in bss */
        int     fib[] = { 0, 1, 1, 2, 3, 5, 8 };    /* stored in data */
extern  char    edata, etext, end;


static int
square(int x)                               /* allocated in stack frame */
{
    int result;                             /* allocated in stack frame */

    result = x * x;

    return x;                               /* passed via register */
}


static void
calculate(int val)                          /* stored in stack frame */
{
    printf("square of %d is %d\n", val, square(val));

    if (val < 1000) {
        int t;                              /* allocated in stack frame */

        t = val * val * val;
        printf("cube of %d is %d\n", val, t);
    }
}


/*
 * segment is an example of where various variables are stored in memory 
 */
int
main(int argc, char *argv[])                /* allocated in stack frame */
{
    static int key = 9973;                  /* data segment */
    static char mbuf[10240000];             /* bss segment */
    size_t text_size;
    size_t data_size;
    size_t bss_size;
    char *p;                                /* allocated in stack frame */

    p = malloc(1024);                       /* points to memory in heap */
    text_size = (size_t)&etext - 1;
    data_size = &edata - &etext;
    bss_size = &end - &edata;

    printf("sizeof unsigned: %d\t\tsizeof address: %d\n", sizeof text_size,
        sizeof &etext);
    printf("last byte of text segment: 0x%08x\n", (&etext) - 1);
    printf("last byte of data segment: 0x%08x\n", (&edata) - 1);
    printf("last byte of bss segment:  0x%08x\n", (&end) - 1);
    printf("size of text segment: %0.1f kb\n", text_size / 1024.0);
    printf("size of data segment: %0.1f kb\n", data_size / 1024.0);
    printf("size of bss segment:  %0.1f kb\n", bss_size / 1024.0);
    calculate(key);

    free(p);

    exit(EXIT_SUCCESS);
}
