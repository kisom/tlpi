/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>

#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

enum E_FIRSTJUMP {
    FIRST = 0,
    SECOND
};

static jmp_buf env;

/*
 * simple function to print hello
 */
static void
hello(void)
{
    printf("hello!\n");
}


/*
 * setjmp function: keeps track of the number of jumps and provides a
 * longjmp() target.
 */
static void
setup_jump(void)
{
    int jumps = -1;
    switch (setjmp(env)) {
    case FIRST:
        jumps++;
        printf("[+] jump target has been set.\n");
        printf("[+] jumps: %d\n", jumps);
        break;
    case SECOND:
        jumps++;
        printf("[+] jumped back in...\n");
        printf("[+] jumps: %d\n", jumps);
        break;
    default:
        printf("[!] *** PANIC ***\n");
        exit(0);
    }
    printf("[+] returning from setup_jump\n");
}


/*
 * jump to a previous setjmp target
 */
static void
jump(void)
{
    longjmp(env, SECOND);
    hello();
}


/*
 * demonstrates jumping into a function that has already returned.
 */
int
main(int argc, char **argv)
{
    /*
     * setup_jump is called twice to demonstrate that the jumps value is
     * reset on a new call to setup_jump, i.e. there should be zero jumps
     * reported after a jump.
     */
    setup_jump();
    setup_jump();
    hello();

    /*
     * due to the restored stack, the number of jumps should be 1 here. this
     * demonstrates that a variable may have an unexpected value when jumping
     * from a function that has already returned.
     */
    jump();
    return 0;
}
