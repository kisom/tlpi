/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

int
main(int argc, char **argv)
{
    int fd;
    off_t off;

    if (argc != 2) {
        printf("usage: %s file\n", argv[0]);
        exit(EX_USAGE);
    }

    printf("[+] open %s\n", argv[1]);
    fd = open(argv[1], O_WRONLY | O_APPEND);    
    if (-1 == fd)
        err(EX_IOERR, "%s", argv[1]);

    printf("[+] lseek()\n");
    off = lseek(fd, 0, SEEK_SET);
    if (off != 0)
        warnx("offset not zero after lseek(2) call");

    printf("[+] write()\n");
    if (4 != write(fd, "test", 4))
        warn("write(2)");

    printf("[+] close()\n");
    if (-1 == close(fd))
        warn("close()");

    return EX_OK;
}
