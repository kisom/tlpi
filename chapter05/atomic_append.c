/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>


int
main(int argc, char **argv)
{
    size_t i, nbytes;
    off_t off;
    int fd, flags, perms;

    flags = O_WRONLY | O_CREAT;
    perms = S_IRUSR | S_IWUSR;
    i = 0;

    if (argc < 3) {
        printf("usage: %s filename nbytes x\n", argv[0]);
        exit(EX_USAGE);
    }

    printf("[+] flags: %d\n", flags);
    nbytes = (size_t)strtoul(argv[2], NULL, 0);
    printf("[+] will write %lu bytes\n", nbytes);

    if (argc == 3) 
        flags = flags | O_APPEND;

    printf("flags:\n");
    if (flags & O_WRONLY)
        printf("\twrite-only\n");
    if (flags & O_CREAT)
        printf("\twill create file if not present\n");
    if (flags & O_APPEND)
        printf("\twill append atomically\n");

    fd = open(argv[1], flags, perms);
    if (-1 == fd)
        err(EX_IOERR, "%s", argv[1]);

    for (i = 0; i < nbytes; ++i) {
        if (!(flags & O_APPEND)) {
            if (1 != write(fd, argv[3], 1))
                warnx("%s: failed to write single byte", argv[1]);
        } else {
            if (1 != write(fd, "x", 1))
                warnx("%s: failed to write single byte", argv[1]);
        }

        if (!(flags & O_APPEND)) {
            off = lseek(fd, 0, SEEK_END);
            printf("[%d] offset: %lld (%c)\n", getpid(), off, argv[3][0]);
        }
    }

    return 0;
}

