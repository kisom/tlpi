/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>

#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include "newuio.h"


/*
 * Perform a read, and scatter it into multiple buffers.
 */
ssize_t 
new_readv(int fd, const struct iovec *iov, int iovcnt)
{
    int i;
    ssize_t readsz, nread;

    readsz = 0;

    for (i = 0; i < iovcnt; ++i) {
        nread = read(fd, iov[i].iov_base, iov[i].iov_len);
        if (nread == -1) {
            readsz = -1;
            break;
        }

        else if ((size_t)nread != iov[i].iov_len)
            warnx("expected %llu bytes, read %lld bytes", 
                  (long long unsigned int)iov[i].iov_len,
                  (long long int)nread);

    }

    return readsz;
}


/*
 * Scatter multiple buffers into a file.
 */
ssize_t 
new_writev(int fd, const struct iovec *iov, int iovcnt)
{
    ssize_t writesz, nwrite;
    int i;

    writesz = 0;
    for (i = 0; i < iovcnt; ++i) {
        nwrite = write(fd, iov->iov_base, iov->iov_len);
        if (-1 == nwrite) {
            writesz = -1;
            errno = EIO;
            break;
        } 

        else if (nwrite != (ssize_t)iov->iov_len)
            warnx("expected %llu bytes, wrote %lld bytes", 
                  (long long unsigned int)iov->iov_len,
                  (long long int)nwrite);

        iov++;
        writesz += nwrite;
    }

    return writesz;
}


