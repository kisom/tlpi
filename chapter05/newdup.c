/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include "newdup.h"

int 
ndup(int oldd)
{
    int fd = 0;
    
    if (oldd > -1)
        fd = fcntl(oldd, F_DUPFD, 0);
    else
        fd = -1;

    return fd;
}


int
ndup2(int oldd, int newd)
{
    int fd = 0;
    int flags = 0;

    if (oldd == newd) {
        if (-1 == fcntl(newd, F_GETFL, 0)) {
            errno = EBADF;
            fd = -1;
        }
    }

    if (-1 != fd) {
        if (oldd != newd) {
            flags = fcntl(newd, F_GETFL, 0);
            if (-1 != flags) {
                if (-1 == close(newd))
                    fd = -1;
            } else
                fd = fcntl(newd, F_DUPFD, 0);
        } else
            fd = oldd;
    }    

    return fd;
}


