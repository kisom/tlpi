/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

int
main(int argc, char **argv)
{
    off_t off;
    int fd, newfd;
    
    if (argc != 2) {
        printf("usage: %s file\n", argv[0]);
        printf("\tverify dup'd descriptors share offsets and flags\n");
    }

    if (-1 == (fd = open(argv[1], O_RDONLY)))
        err(EX_IOERR, "%s", argv[1]);

    newfd = dup(fd);

    if (fcntl(fd, F_GETFL, 0) != fcntl(newfd, F_GETFL, 0))
        warnx("flags differ");
    else
        printf("[+] flags are the same\n");

    off = lseek(fd, 0, SEEK_CUR);
    if (off != lseek(newfd, 0, SEEK_CUR))
        warnx("offsets differ");
    else
        printf("[+] first offset pairs match\n");

    off = lseek(fd, 10, SEEK_CUR);
    if (off != lseek(newfd, 0, SEEK_CUR))
        warnx("offsets differ");
    else
        printf("[+] second offset pairs match\n");

    if (-1 == close(fd) || -1 == close(newfd))
        warn("close(2)");

    return 0;
}
