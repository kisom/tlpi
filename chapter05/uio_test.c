/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <sys/types.h>
#include <sys/stat.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include "newuio.h"


#define BUF_SIZE        4096


static void      usage(char *);
static int       setup_file(char *);
static int       readv_test(int);
static int       writev_test(int);
static void      dump_file(int);


/*
 * read a file specified on the command line into multiple buffers and
 * write it back out
 */
int
main(int argc, char **argv)
{
    int fd, status;

    status = EX_OK;
    if (argc != 2)
        usage(argv[0]);

    fd = setup_file(argv[1]);
    if (-1 == readv_test(fd))
        warn("readv");

    if (0 == writev_test(fd))
        printf("[+] successfully rewrote to %s\n", argv[1]);

    printf("[+] dumping %s to verify output\n", argv[1]);
    dump_file(fd);
    close(fd);

    if (-1 == unlink(argv[1]))
        warn("unlink");

    return status;
}


/*
 * print a short usage message
 */
void
usage(char *progname)
{
    fprintf(stderr, "usage: %s file\n", progname);
    exit(EX_USAGE);
}


/*
 * prepare the file for tests
 */
int
setup_file(char *filename)
{
    int fd;
    ssize_t write_size;
    char buf[] = "hello.world";

    buf[5] = 42;

    fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd > 0) {
        write_size = write(fd, buf, strlen(buf));
        if ((size_t)write_size != strlen(buf))
            warnx("partial write!");
    }

    return fd;
}


/*
 * test readv
 */
int
readv_test(int fd)
{
    char hello[6] = "";
    char world[6] = "";
    char val;
    int iovcnt, status;
    struct iovec iov[3];
    off_t off;

    val = 0;
    status = 0;
    iovcnt = 3;
    iov[0].iov_base = hello;
    iov[0].iov_len = sizeof hello - 1;
    iov[1].iov_base = &val;
    iov[1].iov_len = 1 * sizeof val;
    iov[2].iov_base = world;
    iov[2].iov_len = sizeof world - 1;

    printf("[+] testing readv\n");
    
    off = lseek(fd, 0, SEEK_SET);
    if (-1 == new_readv(fd, iov, 3)) {
        status = -1;
        errno = EIO;
    }

    else {
        printf("\thello: %s\n", hello);
        printf("\tval: %d\n", val);
        printf("\tworld: %s\n", world);
    }

    return status;
}


/*
 * write the iov struct to a file to test writev
 */
int
writev_test(int fd)
{
    char hello[4] = "dag";
    char world[7] = "wereld";
    char val = 47;
    char bang = 33;
    struct iovec iov[4];
    int iovcnt, status;

    iovcnt = 4;
    status = 0;

    iov[0].iov_base = hello;
    iov[0].iov_len = strlen(hello) * sizeof(char);
    iov[1].iov_base = &val;
    iov[1].iov_len = sizeof val;
    iov[2].iov_base = world;
    iov[2].iov_len = strlen(world) * sizeof(char);
    iov[3].iov_base = &bang;
    iov[3].iov_len = sizeof bang;

    printf("will attempt to write %s|0x%d (%c)|%s|%d (%c)\n", hello, val, val,
        world, bang, bang);
    printf("[+] truncating...\n");
    if (-1 == ftruncate(fd, 0)) {
        status = -1;
        errno = EIO;
    }

    printf("[+] rewinding...\n");
    if (!status && 0 != lseek(fd, 0, SEEK_SET)) {
        status = -1;
        errno = EIO;
    }

    printf("[+] writev...\n");    
    if (-1 == new_writev(fd, iov, iovcnt)) {
        status = -1;
        errno = EIO;
    }

    return status;
}


/*
 * dump the output file to prove what was written
 */
void
dump_file(int fd)
{
    ssize_t readsz = 1;
    char buf[BUF_SIZE];
    off_t off;
    int i;

    off = lseek(fd, 0, SEEK_SET);

    if (0 != off)
        warnx("lseek didn't properly seek to beginning of file!");

    for (i = 0; i < 72; ++i)
        printf("-");
    printf("\n");
    
    while (readsz != 0) {
        memset(buf, 0, BUF_SIZE);
        readsz = read(fd, buf, BUF_SIZE - 1);
        if (readsz)
            printf("%s", buf);
    }

    printf("\n");
    for (i = 0; i < 72; ++i)
        printf("-");
    printf("\n");   
}
