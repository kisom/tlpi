/*
 * Copyright (c) 2011 Kyle Isom <coder@kyleisom.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LOGIN_MAX 32
extern char    *__progname;


/*
 * primitive groups(1) implementation
 */
int
main(int argc, char *argv[])
{
        struct passwd   *pw;
        struct group    *gr;
        char            *grnam;
        gid_t            pgid;
	int              i, rv = 0;

	if (argc == 1) {
		printf("usage: %s login\n", __progname);
		exit(EXIT_FAILURE);
	}

        pw = getpwnam(argv[1]);
        pgid = pw->pw_gid;
        gr = getgrgid(pgid);
        printf("%s ", gr->gr_name);
        
        setgrent();
        while ((gr = getgrent())) {
                if (gr->gr_gid == pgid)
                        continue;
                i = 0;
                while (NULL != gr->gr_mem[i]) {
                        if (0 == strncmp(gr->gr_mem[i], argv[1], LOGIN_MAX)) {
                                printf("%s ", gr->gr_name);
                                break;
                        }
                        i++;
                }
        }

        endgrent();
        printf("\n");

	return rv;
}

